package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
       try {       //Catching for incorrect parameters
    if (y.containsAll(x)) {     //Preliminary checking
        int barrier = 0;        //This variable is controlling the order
        int count = 0;          //This variable is counting the successful findings
        int binary;             //This variable is indicating a successful finding
        for (int i = 0; i < x.size(); i++) {
            binary = 0;
            for (int j = barrier; j < y.size(); j++) {
                if (x.get(i).equals(y.get(j))) {                    //Finding of an equity
                    barrier = j + 1;
                    count++;
                    binary = 1;
                    break;
                }
            }
            if ((binary == 0) || (barrier >= y.size())) break;      //Efficient finishing the search
        }
        if (count == x.size()) return true;             //The conditions are satisfied
        else return false;
    } else return false;
} catch (Exception e) {
    throw new IllegalArgumentException();
}
    }
}
